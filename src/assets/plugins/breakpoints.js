import { computed, onMounted, onUnmounted, ref } from "vue"

export default function () {
  let windowWidth = ref(window.innerWidth)

  const onWidthChange = () => windowWidth.value = window.innerWidth
  onMounted(() => window.addEventListener('resize', onWidthChange))
  onUnmounted(() => window.removeEventListener('resize', onWidthChange))
  
  const type = computed(() => {
    let tipe = '';
    if (windowWidth.value < 640) tipe = 'xs'
    if (windowWidth.value >= 640 && windowWidth.value < 768) tipe = 'sm'
    if (windowWidth.value >= 768 && windowWidth.value < 1024) tipe = 'md'
    if (windowWidth.value >= 1024 && windowWidth.value < 1280) tipe = 'lg'
    if (windowWidth.value >= 1280) tipe = 'xl'
    return tipe
  })

  const width = computed(() => windowWidth.value)

  return { width, type }
}