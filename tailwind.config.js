module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors : {
        pinked : '#EEBECE',
        biru : '#0B24FB'
      }
    }
  },
  plugins: []
}
